from flask import Flask, render_template, request, url_for, jsonify
from werkzeug import secure_filename

app = Flask(__name__)

ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/')
def index():
    return render_template('index.html', title="Upload")


@app.route('/upload', methods=['POST'])
def upload():
    if request.method == 'POST':
        if 'file' not in request.files:
            return jsonify({'type': 'error', 'message': 'No file added'}) 
        file = request.files['file']
        filename = secure_filename(file.filename)
        if filename == '':
            return jsonify({'type': 'error', 'message': 'No file'})
        if file and allowed_file(filename):
            file.save(f"./static/{filename}")
            return jsonify({'type': 'success', 'message': f"./static/{filename}"})
        else:
            return jsonify({'type': 'error', 'message': 'No allowed file'})


if __name__ == '__main__':
    app.run(debug=True, port=80)
